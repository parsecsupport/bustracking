import java.util.List;

import org.mahvine.bustracking.modules.MorphiaModule;
import org.mongodb.morphia.logging.MorphiaLoggerFactory;
import org.mongodb.morphia.logging.slf4j.SLF4JLogrImplFactory;

import com.google.common.collect.Lists;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Stage;

import play.Application;
import play.GlobalSettings;


public class Global extends GlobalSettings{
	
	static {
        MorphiaLoggerFactory.reset();
        MorphiaLoggerFactory.registerLogger(SLF4JLogrImplFactory.class);
    }

    private Injector injector;
    List<Module> modules = Lists.newArrayList();
    @Override
    public void beforeStart(final Application app) {
    	modules.add(new AbstractModule() {
            @Override
            protected void configure() {
                bind(Application.class).toInstance(app);
            }
    	});
    	modules.add(new MorphiaModule());
    }
    @Override
    public void onStart(final Application app) {
        injector = Guice.createInjector(Stage.PRODUCTION, modules);
    }
    
    @Override
    public <A> A getControllerInstance(Class<A> controllerClass) throws Exception {
        return injector.getInstance(controllerClass);
    }
    
}
