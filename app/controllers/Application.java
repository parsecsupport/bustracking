package controllers;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mahvine.bustracking.GreatCircle;
import org.mahvine.bustracking.LocationBroadcaster;
import org.mahvine.bustracking.models.CurrentLocation;
import org.mahvine.bustracking.models.Location;

import play.Logger;
import play.libs.EventSource;
import play.libs.Json;
import play.mvc.Content;
import play.mvc.Controller;
import play.mvc.Result;
import akka.actor.ActorRef;
import views.html.coreadmin.index;
import views.html.coreadmin.partials.content;
import views.html.coreadmin.partials.mapview;

import com.fasterxml.jackson.databind.JsonNode;

public class Application extends Controller {

	public static Map<String,Object> currentLocations = new HashMap<String,Object>();
    
    final static ActorRef locBroadcaster = LocationBroadcaster.instance;

    public static Result index() {
    	List<CurrentLocation> currentLocations = CurrentLocation.find.asList();
    	return ok(index.render(currentLocations));
//    	return TODO;
    }
    
    
    public Result saveLongLat(){
    	JsonNode jsonNodeRequest = request().body().asJson();
    	String longitude = jsonNodeRequest.get("lon").asText();
    	String latitude = jsonNodeRequest.get("lat").asText();
    	String id = jsonNodeRequest.get("deviceId").asText();
    	
    	double lon1 = Double.parseDouble(longitude);
    	double lat1 = Double.parseDouble(latitude);
    	CurrentLocation currentLocation = CurrentLocation.find.field("dId").equal(id).get();
    	
    	if(currentLocation != null){
    		double lon2 = Double.parseDouble(currentLocation.lon);
        	double lat2 = Double.parseDouble(currentLocation.lat);
    		double distance = GreatCircle.computeDistance(lon1, lat1, lon2, lat2);
    		currentLocation.distMeters = (int) (distance * 1000);
    		currentLocation.lon = longitude;
    		currentLocation.lat = latitude;
    		Date lastUpdate = currentLocation.datetime;
    		currentLocation.datetime= new Date();

    		int seconds = (int) ((currentLocation.datetime.getTime() - lastUpdate.getTime())/1000);
    		currentLocation.kph = (int) (currentLocation.distMeters / seconds);
    	}else{
    		currentLocation = new CurrentLocation();
    		currentLocation.datetime= new Date();
    		
    		currentLocation.dId = id;    	
    		currentLocation.distMeters = 0;
    		currentLocation.lon = longitude;
    		currentLocation.lat = latitude;
    		currentLocation.kph = 0;
    	}
    	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		currentLocation.readabledate = formatter.format(currentLocation.datetime);
    	currentLocation.save();
    	
    	
    	Location location = new Location();
    	location.deviceId= id;
    	location.lon = longitude;
    	location.lat = latitude;
    	location.datetime = new Date();
    	location.save();
    	currentLocations.put(id,location);
    	locBroadcaster.tell(currentLocation, null);
    	return ok("yeah boi");
    }
    
    public Result getLongLat(){
    	return ok(Json.toJson(currentLocations));
    }
    
    public Result locationStream(){
    	return ok(new EventSource() {  
            public void onConnected() {
               locBroadcaster.tell(this, null); 
            }
        });
    }
    
    public Result showMapView(){
    	return ok(mapview.render());
//    	return TODO;
    }
    
    public Result showListView(){
    	List<CurrentLocation> currentLocations = CurrentLocation.find.asList();
    	return ok(content.render(currentLocations));
    }
    

}
