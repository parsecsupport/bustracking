package org.mahvine.bustracking;

public class GreatCircle { 
    public static void main(String[] args) { 
        double x1 = Math.toRadians(Double.parseDouble("121.001771"));
        double y1 = Math.toRadians(Double.parseDouble("14.537538"));
        double x2 = Math.toRadians(Double.parseDouble("121.019334"));
        double y2 = Math.toRadians(Double.parseDouble("14.541817"));

       /*************************************************************************
        * Compute using law of cosines
        * 1.062048968781266
        *************************************************************************/
        String answer="";
        Long start = System.currentTimeMillis();
        for(int x=0;x<10000;x++){
        // great circle distance in radians
        double angle1 = Math.acos(Math.sin(x1) * Math.sin(x2)
                      + Math.cos(x1) * Math.cos(x2) * Math.cos(y1 - y2));

        // convert back to degrees
        angle1 = Math.toDegrees(angle1);

        // each degree on a great circle of Earth is 60 nautical miles | 1 nautical mile = 1.852 kilometer
        final double distance1 = 60 * angle1;
        answer=distance1 *1000+"";
        }
        System.out.println(answer + " nautical miles");

        System.out.println(System.currentTimeMillis()-start);

       /*************************************************************************
        * Compute using Haverside formula
        * 1.0620489685388537
        *************************************************************************/
//        double a = Math.pow(Math.sin((x2-x1)/2), 2)
//                 + Math.cos(x1) * Math.cos(x2) * Math.pow(Math.sin((y2-y1)/2), 2);
//
//        // great circle distance in radians
//        double angle2 = 2 * Math.asin(Math.min(1, Math.sqrt(a)));
//
//        // convert back to degrees
//        angle2 = Math.toDegrees(angle2);
//
//        // each degree on a great circle of Earth is 60 nautical miles
//        double distance2 = 60 * angle2;
//
//        answer=distance2+"";
//        }
//        System.out.println(answer + " nautical miles");
//
//        System.out.println(System.currentTimeMillis()-start);


   }
    
    public static double computeDistance(double lon1,double lat1,double lon2,double lat2){
        double a = Math.pow(Math.sin((lon2-lon1)/2), 2)
              + Math.cos(lon1) * Math.cos(lon2) * Math.pow(Math.sin((lat2-lat1)/2), 2);

     // great circle distance in radians
     double angle2 = 2 * Math.asin(Math.min(1, Math.sqrt(a)));

     // convert back to degrees
     angle2 = Math.toDegrees(angle2);
     // each degree on a great circle of Earth is 60 nautical miles
     double distance2 = 60*angle2;
     return (distance2 * 1.852)/60;
    }
    
    
    

}