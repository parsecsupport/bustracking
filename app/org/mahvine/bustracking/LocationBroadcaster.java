package org.mahvine.bustracking;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mahvine.bustracking.models.CurrentLocation;
import org.mahvine.bustracking.models.Location;

import com.google.common.collect.Lists;

import play.Logger;
import play.libs.Akka;
import play.libs.EventSource;
import play.libs.F.Callback0;
import play.libs.Json;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class LocationBroadcaster extends UntypedActor {
	public static ActorRef instance = Akka.system().actorOf(
			Props.create(LocationBroadcaster.class));
	List<EventSource> sockets = Lists.newArrayList();
	static {
        Akka.system().scheduler().schedule(
            Duration.Zero(),
            Duration.create(5000, MILLISECONDS),
            instance, "TICK",  Akka.system().dispatcher(),
            null
        );
    }
	public void onReceive(Object message) {
		// Handle connections
		if (message instanceof EventSource) {
			final EventSource eventSource = (EventSource) message;
			if (sockets.contains(eventSource)) {
				// Browser is disconnected
				sockets.remove(eventSource);
				Logger.info("Browser disconnected (" + sockets.size()
						+ " browsers currently connected)");
			} else {
				
				// Register disconnected callback
				eventSource.onDisconnected(new Callback0(){
					@Override
					public void invoke() throws Throwable {
						Logger.info("disconnected!");
						getContext().self().tell(eventSource, null);
					}
					
				});
					
				// New browser connected
				sockets.add(eventSource);
				Logger.info("New browser connected (" + sockets.size()
						+ " browsers currently connected)");
			}

		}
		if (message instanceof CurrentLocation) {
			// Send the location event to all EventSource sockets
			List<EventSource> shallowCopy = new ArrayList<EventSource>(sockets); 
			CurrentLocation location = (CurrentLocation) message;
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			String currentLocationEvent = "currentLocation|"+location.dId+"|"+location.distMeters+"|"+location.lon+"|"+location.lat+"|"+formatter.format(location.datetime)+"|"+location.kph;
			for (EventSource es : shallowCopy) {
				es.sendData(currentLocationEvent);
			}

		}
		if (message.equals("TICK")) {
			// Send the location event to all EventSource sockets
			List<EventSource> shallowCopy = new ArrayList<EventSource>(sockets); 
			for (EventSource es : shallowCopy) {
				es.sendData("tick");
			}

		}

	}

}
