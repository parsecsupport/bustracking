package org.mahvine.bustracking.models;

import java.util.Date;

import org.mahvine.bustracking.modules.morphia.Model;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Property;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(value = "currentlocation", noClassnameStored = true, concern = "NORMAL")
public class CurrentLocation extends Model{

	@Property("dId")
    @Indexed(unique = true)
	public String dId;
	
	@Property("lon")
	public String lon;
	
	@Property("lat")
	public String lat;

	@Property("date")
	public Date datetime;
	
	@Property("readdate")
	public String readabledate;

	@Property("kph")
	public int kph;
	
	@Property("dist")
	public int distMeters;
	
	
	public static final Finder<CurrentLocation> find = new Finder<CurrentLocation>(CurrentLocation.class);
	
}
