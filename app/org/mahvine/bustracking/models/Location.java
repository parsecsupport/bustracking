package org.mahvine.bustracking.models;



import java.util.Date;

import org.mahvine.bustracking.modules.morphia.Model;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Property;

@Entity(value = "location", noClassnameStored = true, concern = "NORMAL")
public class Location extends Model{

	@Property("deviceId")
	@Indexed(unique=false)
	public String deviceId;

	@Property("lon")
	public String lon;
	@Property("lat")
	public String lat;
	@Property("date")
	public Date datetime;

	public static final Finder<Location> find = new Finder<Location>(Location.class);

	
}
