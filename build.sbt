name := "bustracking"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache
  ,//Morphia
  "org.mongodb" % "mongo-java-driver" % "2.11.4",
  "org.mongodb.morphia" % "morphia" % "0.106",
  "org.mongodb.morphia" % "morphia-logging-slf4j" % "0.106"
  ,// Guice
  "com.google.inject" % "guice" % "3.0",
  "com.google.inject.extensions" % "guice-assistedinject" % "3.0",
  "com.google.inject.extensions" % "guice-multibindings" % "3.0",
  "com.google.inject.extensions" % "guice-throwingproviders" % "3.0"
)     

play.Project.playJavaSettings
